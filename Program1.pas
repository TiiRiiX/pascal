uses GraphABC;

Var n,h,w,i:integer;
    t:array[1..3] of byte;

Procedure Draw(a,b,c,d:integer);
var i,j:integer;
begin
  for i:=a to c do
  for j:=b to d do
  PutPixel(i,j,clRed);
end;

Procedure Hanoi(n:byte; a,b,c:byte);
begin
  if n>0 then
  begin
    Hanoi(n-1,a,c,b);
    Writeln(a,'=>',b);
    t[a]:=t[a]-1;
    
    t[b]:=t[b]-1;
    Hanoi(n-1,c,b,a);
  end;
end;

begin
  Write('������� ���-�� ������: ');
  Readln(n);
  t[1]:=n;
  h:=300 div n;
  w:=100;
  for i:=1 to 3 do
  Draw(50+w*(i-1)+20*(i-1),300,50+w*(i-1)+w+20*(i-1),320);
  for i:=n downto 1 do
  Draw(50+w*(i-1)+20*(i-1)+10*(n-i+1),300-h*i,50+w*(i-1)+20*(i-1)-10*(n-i+1),300-h*i+20);
   
  Hanoi(n,1,2,3);
end.
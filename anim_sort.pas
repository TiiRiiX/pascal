uses
  GraphABC;

const n = 500;

type
  mas = array[1..n] of integer;

var
  x: mas;
  max,w_n,w_h: integer;
  w_h3:real;

procedure Input(var a: mas);
var
  i: integer;
begin
  Writeln('��������� ���������� �������...');
  for i := 1 to n do
  begin
    a[i] := Random(2 * n) - n;
    if a[i] > max then max := a[i];
    //Write(a[i],' ');
  end;
  Writeln();
  if max <= 0 then max := 1;
end;

procedure Anime(a: mas; i, j, y1: integer);
var
  g, d: integer;
begin
  if (j <> i) then
  begin
    for g := 1 to w_n do
      if x[i] > 0 then
        for d := floor(x[i] * w_h3) downto 1 do
          PutPixel(-w_n + i * w_n + g, y1 - d, clWhite)
      else 
        for d := 1 downto floor(x[i] * w_h3) do
          PutPixel(-w_n + i * w_n + g, y1 - d, clWhite);
    
    for g := 1 to w_n do
      if x[j] > 0 then
        for d := floor(x[j] * w_h3) downto 1 do
          PutPixel(-w_n + i * w_n + g, y1 - d, clBlue)
      else 
        for d := 1 downto floor(x[j] * w_h3) do
          PutPixel(-w_n + i * w_n + g, y1 - d, clRed);
    
    for g := 1 to w_n do
      if x[j] > 0 then
        for d := floor(x[j] * w_h3) downto 1 do
          PutPixel(-w_n + j * w_n + g, y1 - d, clWhite)
      else 
        for d := 1 downto floor(x[j] *w_h3) do
          PutPixel(-w_n + j * w_n + g, y1 - d, clWhite);
    
    for g := 1 to w_n do
      if x[i] > 0 then
        for d := floor(x[i] * w_h3) downto 1 do
          PutPixel(-w_n + j * w_n + g, y1 - d, clBlue)
      else 
        for d := 1 downto floor(x[i] * w_h3) do
          PutPixel(-w_n + j * w_n + g, y1 - d, clRed);
  end;
end;


procedure Sort(var a: mas; l, r: integer);
var
  i, j, t, m: integer;
begin
  i := l;j := r;m := a[(l + r) div 2];
  repeat
    while a[i] < m do i := i + 1;
    while a[j] > m do j := j - 1;
    if i <= j then
    begin Anime(a, i, j,w_h); t := a[i];a[i] := a[j];a[j] := t;i := i + 1;j := j - 1; end;
  until i > j;
  if i < r then Sort(a, i, r);
  if j > l then Sort(a, l, j); 
end;

procedure Sort2(var a: mas);
var
  i, t, k: integer;
  f: boolean;
begin
  k := n;
  f := false;
  while (k >= 1) and (f = false) do 
  begin
    f := true;
    for i := 1 to k - 1 do
      if a[i] > a[i + 1] then begin Anime(a, i, i + 1, w_h); t := a[i];a[i] := a[i + 1];a[i + 1] := t; f := false; end;
    k := k - 1;
  end;
end;


procedure Output(a: mas);
var
  i: integer;
begin
  Writeln('����� ����������� �������...');
  for i := 1 to n do
    //Write(a[i],' ');
    Writeln();
end;

procedure Draw(a: mas; x1, y1: integer);
var
  i, g, d: integer;
begin
  for i := 1 to n do
    for g := 1 to w_n do
      if x[i] > 0 then
        for d := floor(x[i] * w_h3) downto 1 do
          PutPixel(-w_n + i * w_n + g, y1 - d, clBlue)
      else 
        for d := 1 downto floor(x[i] * w_h3) do
          PutPixel(-w_n + i * w_n + g, y1 - d, clRed);
end;


begin
  SetWindowWidth(500);
  w_n:=Window.Width div n;  
  w_h:=Window.Height div 2;
  Input(x);
  w_h3:=Window.Height / (max * 3);
  Draw(x, 0, w_h);
  Sort(x,1,n);
  //Sort2(x);
  Output(x);
end.
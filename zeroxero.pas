uses GraphABC;

type mas = array[1..3,1..3] of byte;

var pol:mas;
    win,upr:byte;

Procedure Restangle(x1,x2,y1,y2:integer);
var d,g:integer;
begin
for d:=x1 to x2 do
for g:=y1 to y2 do
PutPixel(d,g,clBlack);
end;

Procedure Plus(i,j:integer);
begin
Restangle(20+(i-1)*100,20+(i-1)*100+55,50+(j-1)*100,50+(j-1)*100+5);
Restangle(47+(i-1)*100,46+(i-1)*100+5,20+(j-1)*100,20+(j-1)*100+60);
end;

Procedure Minus(i,j:integer);
begin
Restangle(20+(i-1)*100,20+(i-1)*100+55,50+(j-1)*100,50+(j-1)*100+5);
end;


Procedure Draw_Ground();
begin
Restangle(100,102,0,300);
Restangle(200,202,0,300);
Restangle(0,300,100,102);
Restangle(0,300,200,202);
end;

Procedure Check(pol:mas);
begin
  if ((pol[1,1]=1) and (pol[1,2]=1) and (pol[1,3]=1))
  or ((pol[2,1]=1) and (pol[2,2]=1) and (pol[2,3]=1))
  or ((pol[3,1]=1) and (pol[3,2]=1) and (pol[3,3]=1))
  or ((pol[1,1]=1) and (pol[2,1]=1) and (pol[3,1]=1))
  or ((pol[1,2]=1) and (pol[2,2]=1) and (pol[3,2]=1))
  or ((pol[1,3]=1) and (pol[2,3]=1) and (pol[3,3]=1))
  or ((pol[1,1]=1) and (pol[2,2]=1) and (pol[3,3]=1))
  or ((pol[1,3]=1) and (pol[2,2]=1) and (pol[3,1]=1))
  then win:=1;
  
  if ((pol[1,1]=2) and (pol[1,2]=2) and (pol[1,3]=2))
  or ((pol[2,1]=2) and (pol[2,2]=2) and (pol[2,3]=2))
  or ((pol[3,1]=2) and (pol[3,2]=2) and (pol[3,3]=2))
  or ((pol[1,1]=2) and (pol[2,1]=2) and (pol[3,1]=2))
  or ((pol[1,2]=2) and (pol[2,2]=2) and (pol[3,2]=2))
  or ((pol[1,3]=2) and (pol[2,3]=2) and (pol[3,3]=2))
  or ((pol[1,1]=2) and (pol[2,2]=2) and (pol[3,3]=2))
  or ((pol[1,3]=2) and (pol[2,2]=2) and (pol[3,1]=2))
  then win:=2;
  
   if (pol[1,1]<>0) and (pol[1,2]<>0) and (pol[1,3]<>0)
  and (pol[2,1]<>0) and (pol[2,2]<>0) and (pol[2,3]<>0)
  and (pol[3,1]<>0) and (pol[3,2]<>0) and (pol[3,3]<>0)
  and (win=0) then begin win:=3; Writeln('� ��� ����� :('); end;
  
  if win=1
    then Writeln('����� ��������!');
  if win=2
    then Writeln('������ ��������!');
  end;
 
Procedure Step_AI(pol:mas;Var i,j:integer);
begin
  if (pol[2,3]=0) then begin j:=2; i:=3; end;
  if (pol[3,2]=0) then begin j:=3; i:=2; end;
  if (pol[1,2]=0) then begin j:=1; i:=2; end;
  if (pol[2,1]=0) then begin j:=2; i:=1; end;
  if (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[3,1]=0) then begin j:=3; i:=1; end;
  if (pol[1,3]=0) then begin j:=1; i:=3; end;
  if (pol[1,1]=0) then begin j:=1; i:=1; end;
  if (pol[2,2]=0) then begin j:=2; i:=2; end;
  
  if (pol[3,2]=1) and (pol[1,1]=1) and (pol[2,2]=2) and (pol[3,1]=0) then begin j:=3; i:=1; end;
  
  if (pol[2,2]=2) and (pol[1,1]=1) and (pol[3,3]=1) and (pol[2,3]=0) then begin j:=2; i:=3; end;
  if (pol[2,2]=2) and (pol[1,3]=1) and (pol[3,1]=1) and (pol[2,3]=0) then begin j:=2; i:=3; end;
  if (pol[2,2]=1) and (pol[3,1]=0) and (pol[3,2]=0) then begin j:=3; i:=1; end;
  if (pol[2,2]=1) and (pol[1,3]=1) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  
  if (pol[1,3]=1) and (pol[2,3]=1) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[1,3]=1) and (pol[2,3]=0) and (pol[3,3]=1) then begin j:=2; i:=3; end;
  if (pol[1,3]=0) and (pol[2,3]=1) and (pol[3,3]=1) then begin j:=1; i:=3; end;
  
  if (pol[1,1]=1) and (pol[2,2]=1) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[1,1]=1) and (pol[2,2]=0) and (pol[3,3]=1) then begin j:=2; i:=2; end;
  if (pol[1,1]=0) and (pol[2,2]=1) and (pol[3,3]=1) then begin j:=1; i:=1; end;
  
  if (pol[3,1]=1) and (pol[2,2]=1) and (pol[1,3]=0) then begin j:=1; i:=3; end;
  if (pol[3,1]=1) and (pol[2,2]=0) and (pol[1,3]=1) then begin j:=2; i:=2; end;
  if (pol[3,1]=0) and (pol[2,2]=1) and (pol[1,3]=1) then begin j:=3; i:=1; end;  
  
  if (pol[1,1]=1) and (pol[1,2]=1) and (pol[1,3]=0) then begin j:=1; i:=3; end;
  if (pol[1,1]=1) and (pol[1,2]=0) and (pol[1,3]=1) then begin j:=1; i:=2; end;
  if (pol[1,1]=0) and (pol[1,2]=1) and (pol[1,3]=1) then begin j:=1; i:=1; end;
  
  if (pol[2,1]=1) and (pol[2,2]=1) and (pol[2,3]=0) then begin j:=2; i:=3; end;
  if (pol[2,1]=1) and (pol[2,2]=0) and (pol[2,3]=1) then begin j:=2; i:=2; end;
  if (pol[2,1]=0) and (pol[2,2]=1) and (pol[2,3]=1) then begin j:=2; i:=1; end;
  
  if (pol[3,1]=1) and (pol[3,2]=1) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[3,1]=1) and (pol[3,2]=0) and (pol[3,3]=1) then begin j:=3; i:=2; end; 
  if (pol[3,1]=0) and (pol[3,2]=1) and (pol[3,3]=1) then begin j:=3; i:=1; end;
  
  if (pol[1,1]=1) and (pol[2,1]=1) and (pol[3,1]=0) then begin j:=3; i:=1; end;
  if (pol[1,1]=1) and (pol[2,1]=0) and (pol[3,1]=1) then begin j:=2; i:=1; end;
  if (pol[1,1]=0) and (pol[2,1]=1) and (pol[3,1]=1) then begin j:=1; i:=1; end;
  
  if (pol[1,2]=1) and (pol[2,2]=1) and (pol[3,2]=0) then begin j:=3; i:=2; end;
  if (pol[1,2]=1) and (pol[2,2]=0) and (pol[3,2]=1) then begin j:=2; i:=2; end;
  if (pol[1,2]=0) and (pol[2,2]=1) and (pol[3,2]=1) then begin j:=1; i:=2; end;
  
  if (pol[1,3]=1) and (pol[2,3]=1) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[1,3]=1) and (pol[2,3]=0) and (pol[3,3]=1) then begin j:=2; i:=3; end;
  if (pol[1,3]=0) and (pol[2,3]=1) and (pol[3,3]=1) then begin j:=1; i:=3; end;

  if (pol[1,1]=2) and (pol[1,2]=2) and (pol[1,3]=0) then begin j:=1; i:=3; end;
  if (pol[1,1]=2) and (pol[1,2]=0) and (pol[1,3]=2) then begin j:=1; i:=2; end;
  if (pol[1,1]=0) and (pol[1,2]=2) and (pol[1,3]=2) then begin j:=1; i:=1; end;
  
  if (pol[2,1]=2) and (pol[2,2]=2) and (pol[2,3]=0) then begin j:=2; i:=3; end;
  if (pol[2,1]=2) and (pol[2,2]=0) and (pol[2,3]=2) then begin j:=2; i:=2; end;
  if (pol[2,1]=0) and (pol[2,2]=2) and (pol[2,3]=2) then begin j:=2; i:=1; end;
  
  if (pol[3,1]=2) and (pol[3,2]=2) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[3,1]=2) and (pol[3,2]=0) and (pol[3,3]=2) then begin j:=3; i:=2; end;
  if (pol[3,1]=0) and (pol[3,2]=2) and (pol[3,3]=2) then begin j:=3; i:=1; end;
  
  if (pol[1,1]=2) and (pol[2,1]=2) and (pol[3,1]=0) then begin j:=3; i:=1; end;
  if (pol[1,1]=2) and (pol[2,1]=0) and (pol[3,1]=2) then begin j:=2; i:=1; end;
  if (pol[1,1]=0) and (pol[2,1]=2) and (pol[3,1]=2) then begin j:=1; i:=1; end;
  
  if (pol[1,2]=2) and (pol[2,2]=2) and (pol[3,2]=0) then begin j:=3; i:=2; end;
  if (pol[1,2]=2) and (pol[2,2]=0) and (pol[3,2]=2) then begin j:=2; i:=2; end;
  if (pol[1,2]=0) and (pol[2,2]=2) and (pol[3,2]=2) then begin j:=1; i:=2; end;
  
  if (pol[1,3]=2) and (pol[2,3]=2) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[1,3]=2) and (pol[2,3]=0) and (pol[3,3]=2) then begin j:=2; i:=3; end;
  if (pol[1,3]=0) and (pol[2,3]=2) and (pol[3,3]=2) then begin j:=1; i:=3; end;
  
  if (pol[1,1]=2) and (pol[2,2]=2) and (pol[3,3]=0) then begin j:=3; i:=3; end;
  if (pol[1,1]=2) and (pol[2,2]=0) and (pol[3,3]=2) then begin j:=2; i:=2; end;
  if (pol[1,1]=0) and (pol[2,2]=2) and (pol[3,3]=2) then begin j:=1; i:=1; end;
  
  if (pol[3,1]=2) and (pol[2,2]=2) and (pol[1,3]=0) then begin j:=1; i:=3; end;
  if (pol[3,1]=2) and (pol[2,2]=0) and (pol[1,3]=2) then begin j:=2; i:=2; end;
  if (pol[3,1]=0) and (pol[2,2]=2) and (pol[1,3]=2) then begin j:=3; i:=1; end;
  
end;

Procedure Step(Var pol:mas);
var i,j:integer;
begin
while win=0 do
begin
if win=0 then
begin
  repeat
  Readln(i,j);
  //i:=Random(3)+1;j:=Random(3)+1;
  if (j>3) or (j<=0) or (i>3) or (i<=0)
  then Writeln('�������� ���������� ������')
  else
  if (pol[j,i]=1) or (pol[j,i]=2)
  then Writeln('��� ������ ��� ������');
  until ((j<=3)and(j>=1) and ((i<=3)and(i>=1)) and (pol[j,i]=0));
  pol[j,i]:=1;
  Plus(i,j);
  Check(pol);
end;
if win=0 then
begin
  repeat
  //Readln(i,j);
  //i:=Random(3)+1;j:=Random(3)+1;
  Step_AI(pol,i,j);
  if (pol[j,i]=1) or (pol[j,i]=2)
  then Writeln('��� ������ ��� ������');
  until pol[j,i]=0;
  pol[j,i]:=2;
  Minus(i,j);
  Check(pol);
end;
end;
end;

Procedure ClearPol(Var pol:mas);
var i,j:integer;
begin
for j:=1 to 3 do
for i:=1 to 3 do
pol[j,i]:=0;
end;

begin
upr:=1;
repeat
win:=0;

ClearPol(pol);
ClearWindow();
SetFontSize(6);
CenterWindow();
Draw_Ground();
Step(pol);
Write('��������� (0 - ���, 1 - ��)?');readln(upr);
until upr<>1;
CloseWindow();
end.
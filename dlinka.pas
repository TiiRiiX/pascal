const NMAX = 100;
type lint = record
				arr: array[1..NMAX] of integer;
				len: byte;
				sign: boolean;
			end;
var x,y: lint;

procedure lint_input(var l: lint;s: string);
var i,k,t: integer;
begin
	for i:=1 to NMAX do l.arr[i]:=0;
	l.sign:=s[1]='-';
	if l.sign then delete(s,1,1);
	t:=0;
	if (length(s) mod 4)<>0 then begin
		t:=1;
		val(copy(s,1,length(s) mod 4),l.arr[1],k);
		delete(s,1,length(s) mod 4);
	end;
	l.len:=(length(s) div 4)+t;
	for i:=1+t to (length(s) div 4)+t do begin
		val(copy(s,1,4),l.arr[i],k);
		delete(s,1,4);
	end;
	for i:=1 to l.len div 2 do begin
		k:=l.arr[i];
		l.arr[i]:=l.arr[l.len-i+1];
		l.arr[l.len-i+1]:=k;
	end;
end;

function lint_tostring(n: lint): string;
var s,ss: string; i: integer;
begin
	if n.sign then s:='-' else s:='';
	for i:=n.len downto 1 do begin
		str(n.arr[i],ss);
		if i<n.len then while length(ss)<4 do ss:='0'+ss;
		s:=s+ss;
	end;
	lint_tostring:=s;
end;

function lint_abs(n: lint): lint;
var l: lint;
begin
	l:=n;
	l.sign:=false;
	lint_abs:=l;
end;

function lint_comp(n1: lint; n2: lint): char;
var rez: char; i: integer;
begin
	if n1.sign then begin
		if n2.sign then begin
			if n1.len<n2.len then rez:='>'
			else if n1.len>n2.len then rez:='<'
				else begin
					i:=n1.len;
					rez:='=';
					while (i>=1) and (rez='=') do begin
						if n1.arr[i]<n2.arr[i] then rez:='>'
						else if n1.arr[i]>n2.arr[i] then rez:='<';
						i:=i-1;
					end;
				end;
		end
		else rez:='<'
	end
	else begin
		if not(n2.sign) then begin
			if n1.len<n2.len then rez:='<'
			else if n1.len>n2.len then rez:='>'
				else begin
					i:=n1.len;
					rez:='=';
					while (i>=1) and (rez='=') do begin
						if n1.arr[i]<n2.arr[i] then rez:='<'
						else if n1.arr[i]>n2.arr[i] then rez:='>';
						i:=i-1;
					end;
				end;
		end
		else rez:='>'
	end;
	lint_comp:=rez;
end;

function lint_plus(n1: lint; n2: lint): lint;
var i,p: integer; rez: lint;
begin
	if lint_comp(lint_abs(n1),lint_abs(n2))='<' then begin
		rez:=n1;
		n1:=n2;
		n2:=rez;
	end;
	rez:=n1;
	p:=0;
	rez.sign:=(n1.sign and n2.sign) or (n1.sign and (lint_comp(lint_abs(n1),n2)='>')) or (n2.sign and (lint_comp(lint_abs(n2),n1)='>'));
	if n1.sign then begin
		n1.sign:=not n1.sign;
		n2.sign:=not n2.sign;
	end;
	if n2.sign then begin
		for i:=1 to n1.len do begin
			rez.arr[i]:=(n1.arr[i]-n2.arr[i]-p) mod 10000;
			if rez.arr[i]<0 then begin
				rez.arr[i]:=rez.arr[i]+10000;
				p:=1;
			end
			else p:=0;
		end;
		i:=nmax;
		while (rez.arr[i]=0) and (i>1) do i:=i-1;
		rez.len:=i;
	end
	else begin
		for i:=1 to n1.len do begin
			rez.arr[i]:=(n1.arr[i]+n2.arr[i]+p) mod 10000;
			p:=(n1.arr[i]+n2.arr[i]+p) div 10000;
		end;
		rez.len:=n1.len;
		if p<>0 then begin
			rez.len:=rez.len+1;
			rez.arr[rez.len]:=p;
		end;
		for i:=rez.len+1 to NMAX do rez.arr[i]:=0;
	end;
	lint_plus:=rez;
end;

function lint_minus(n1: lint; n2: lint): lint;
begin
	n2.sign:=not n2.sign;
	lint_minus:=lint_plus(n1,n2);
end;

function lint_multy(n1:lint;n2:lint):lint;
var i:integer;d:lint;
begin

end;

begin
  Readln(s);
	lint_input(x,s);
	Readln(s);
	lint_input(y,s);
	writeln(lint_tostring(x),' ',lint_tostring(y),' ',lint_tostring(lint_multy(x,y)));
end.

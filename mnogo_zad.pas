const n=5;
const m=5;

type mas = array[1..m,1..n] of integer;

var x:mas;

Procedure  Input(Var a:mas);
var i,j:integer;
begin
Writeln('���� ��������� �������...');
for j:=1 to m do
begin
for i:=1 to n do
begin
a[j,i]:=Random(2*n)-n;
Write(a[j,i],' ');
end;
Writeln();
end;
end;

Procedure Solve(Var a:mas);
var g,k,t,i,j:integer;
    f:boolean;
    b:array[1..n*m] of integer;
begin
g:=1;
for j:=1 to m do
for i:=1 to n do
begin
b[g]:=a[j,i];
g:=g+1;
end;
k:=m*n;
f:=false;
While (k>=1) and (f=false) do 
begin
f:=true;
for i:=1 to k-1 do
if b[i]>b[i+1] then begin t:=b[i];b[i]:=b[i+1];b[i+1]:=t; f:=false; end;
k:=k-1;
end;
g:=1;
for j:=1 to m do
for i:=1 to n do
begin
a[j,i]:=b[g];
g:=g+1;
end;
end;

Procedure Solve2(a:mas);

type main = record i:integer;
             j:integer;
             z:integer;
             end;
var i,j:integer;
    max,min:main;
begin
max.i:=1;
max.j:=1;
max.z:=a[1,1];
min.i:=1;
min.j:=1;
min.z:=a[1,1];
for j:=1 to m do
for i:=1 to n do
begin
if a[j,i]<min.z then begin min.z:=a[j,i]; min.i:=i;min.j:=j; end;
if a[j,i]>max.z then begin max.z:=a[j,i]; max.i:=i;max.j:=j; end;
end;
Writeln('������������ ������� ������� = ',max.z,' (',max.j,',',max.i,')');
Writeln('����������� ������� ������� = ',min.z,' (',min.j,',',min.i,')');
end;

Procedure Solve3(a:mas);
type main = record 
            z:integer;
            j:integer;
            end;
var i,j,s:integer;
    max,min:main;
begin
max.z:=-MaxInt;
min.z:=maxInt;
for j:=1 to m do
begin
s:=0;
for i:=1 to n do
s:=s+a[j,i];
if s>max.z then begin max.z:=s; max.j:=j; end;
if s<min.z then begin min.z:=s; min.j:=j; end;
end;
Write('Max: (');for i:=1 to n-1 do Write(a[max.j,i],' '); Write(a[max.j,n],')'); Writeln(' S = ',max.z);
Write('Min: (');for i:=1 to n-1 do Write(a[min.j,i],' '); Write(a[min.j,n],')'); Writeln(' S = ',min.z);
end;

Procedure Solve4 (Var a:mas);
var i,j:integer;
begin
Writeln('���������� �����������...');
for j:=1 to m do
for i:=1 to n do
if a[j,i]>0 then a[j,i]:=1 else a[j,i]:=0;
for j:=1 to m do
begin
for i:=1 to n do
if i<=j then Write(a[j,i],' ') else Write(' ');
Writeln();
end;
end;

Procedure Output(a:mas);
var i,j:integer;
begin
Writeln('����� �������...');
for j:=1 to m do
begin
for i:=1 to n do
Write(a[j,i],' ');
Writeln();
end;
end;

begin
Input(x);
Solve(x);
Solve2(x);
Solve3(x);
Solve4(x);
Output(x);
end.